-- you only need to make this assertion if using AddObjectTypeRespawn,
-- custom FOF checks can be loaded in any order
assert(AddObjectTypeRespawn, "This must be loaded after respawn.lua!")


-- more complicated example: spikes and wall spikes

-- this is used for shouldrespawnfunc
-- forces either type of spike to always respawn
local function StatueShouldRespawn()
	return true
end

-- this is used for timerfunc
-- forces either type of spike to respawn in 3 seconds
local function StatueTimer()
	return 40*TICRATE
end

AddObjectTypeRespawn(
	-- objecttype
	MT_SUSPICIOUSFACESTABBERSTATUE,

	-- shouldrespawnfunc
	StatueShouldRespawn,

	-- timerfunc
	StatueTimer,

	-- spawnfunc (use default RegularSpawn)
	nil,

	-- setupfunc
	function(spawnpoint, mo)
		if not (spawnpoint and spawnpoint.valid and mo and mo.valid) then return end

		mo.angle = FixedAngle(spawnpoint.angle << FRACBITS)

		if spawnpoint.options & MTF_OBJECTFLIP
			mo.eflags = $|MFE_VERTICALFLIP
			mo.flags2 = $|MF2_OBJECTFLIP
		end
	end
)